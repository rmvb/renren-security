import org.apache.commons.lang.WordUtils;
import org.junit.Test;

/**
 * @author zhangheng
 * @create 2018-04-02 14:31
 */
public class Tester {

    @Test
    public void test01() {
        String columnName = "user_id";
        String s = WordUtils.capitalizeFully(columnName, new char[]{'_'});
        System.out.println(s);
        String replace = s.replace("_", "");
        System.out.println(replace);
    }

}
